#!/bin/bash

set -eu
set -o pipefail

BMO_RELEASE=v0.8.0
KUSTOMIZE_VERSION=${KUSTOMIZE_VERSION:-4.5.4}
export BMO_RELEASE
export KANOD_STACK_VERSION="${STACK_VERSION%.*([0-9])}"

basedir=$(dirname "${BASH_SOURCE[0]}")
builddir="${basedir}/build"
mkdir -p "${builddir}"

target="${builddir}/stack.yml"

echo '- Loading kustomize'
curl -L -o kustomize.tgz https://github.com/kubernetes-sigs/kustomize/releases/"download/kustomize%2Fv${KUSTOMIZE_VERSION}/kustomize_v${KUSTOMIZE_VERSION}_linux_amd64.tar.gz"
tar -zxvf kustomize.tgz kustomize
rm kustomize.tgz

echo 'Building main stack'
# shellcheck disable=SC2016
./kustomize build "${basedir}"/manifests | sed "s!\\\${KANOD_STACK_VERSION}!${KANOD_STACK_VERSION}!g" | sed "s!\\\${BMO_RELEASE}!${BMO_RELEASE}!" > "${target}"

# We do not enable ClusterClass as we have our own abstractions
# shellcheck disable=SC2016
sed -i 's/\${CLUSTER_TOPOLOGY:=false}/false/' "${target}"
# We do not use ignition for bootstrap
# shellcheck disable=SC2016
sed -i 's/\${EXP_KUBEADM_BOOTSTRAP_FORMAT_IGNITION:=false}/false/' "${target}"
# We set MARIADB_HOST_IP to localhost (it is not a variable !)
sed -i 's/MARIADB_HOST_IP/127.0.0.1/' "${target}"
# We set IRONIC_HOST_IP to variable IRONIC_IP
# All the ${pass:-""} keep ${pass}
sed -i 's/\${\([A-Z0-9_]*\):-""}/${\1}/' "${target}"
# All the ${flag:='false'} to false
sed -i 's/\${\([A-Z0-9_]*\):='"'"'false'"'"'}/'"'"'false'"'"'/' "${target}"
# All the IRONIC_NO_XXX:-path, keep path
sed -i 's/\${[A-Z0-9_]*:-\([^}]*\)}/\1/' "${target}"
# Quote variables occuring alone
sed -i "s/: \\(\\\${.*}\\)\$/: '\\1'/" "${target}"

echo 'Buildding components:'
for path in ironic bmoperator bm-crd capi/overlays/capi capi/overlays/capi-hosts hostclaim host-baremetal host-kubevirt kubevirt nfs-provisioner clusterdef dashboard baremetalpool network-operator rke2 external brokerdef brokernet kamaji bmh-data kea ipam openstack rook rook-ceph; do
   echo "- ${path}"
   entry="$(basename "${path}")"
   # shellcheck disable=SC2016
   ./kustomize build "${basedir}/manifests/${path}" | sed "s!\\\${KANOD_STACK_VERSION}!${KANOD_STACK_VERSION}!g" | sed "s!\\\${BMO_RELEASE}!${BMO_RELEASE}!" > "${basedir}/build/kn-${entry}.yml"
done

# argocd version cmp and krm
echo "- argocd/overlays/cmp"
./kustomize build "${basedir}/manifests/argocd/overlays/cmp" > "${basedir}/build/kn-argocd-cmp.yml"
echo "- argocd/overlays/krm"
./kustomize build "${basedir}/manifests/argocd/overlays/krm" > "${basedir}/build/kn-argocd-krm.yml"

sed -i 's/MARIADB_HOST_IP/127.0.0.1/' "${basedir}/build/kn-ironic.yml"

# shellcheck disable=SC2016
./kustomize build "${basedir}/kanod-operator/config/kanod" | sed "s!\\\${KANOD_STACK_VERSION}!${KANOD_STACK_VERSION}!g" > "${basedir}/build/kanod-operator.yml"

# create manifests for ingress
./kustomize build "${basedir}/manifests/ingress/floating" > "${basedir}/build/kanod-ingress.yml"

# create manifests for registrar
./kustomize build "${basedir}/manifests/registrar" > "${basedir}/build/tpm-registrar.yml"

# Add manifest for cert-manager and metallb
./kustomize build "${basedir}"/manifests/cert-manager > "${basedir}/build/cert-manager.yml"

# Adds applications to result
./kustomize build  "${basedir}/manifests/applications" > "${basedir}/build/applications.yml"
cp "${basedir}/manifests/applications/application-baremetals.yaml" "${basedir}/build/kn-app-baremetals.yml"
cp "${basedir}/manifests/applications/application-projects.yaml" "${basedir}/build/kn-app-projects.yml"
cp "${basedir}/manifests/applications/application-network.yaml" "${basedir}/build/kn-app-network.yml"
