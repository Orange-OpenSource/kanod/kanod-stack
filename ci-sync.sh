#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu
set -o pipefail

# We need the real registry (without path)
# shellcheck disable=SC2001
REGISTRY="$(sed 's!/.*$!!' <<< "$EXTERNAL_REGISTRY")"
echo "Login on local registry"
skopeo login --tls-verify=false --username "${REGISTRY_USER}" \
    --password-stdin "${REGISTRY}" <<< "${REGISTRY_PASSWORD}"

function kanod_image_sync() {
  echo "- sync of $1"
  for source in $(grep -o 'image: .*' "$1" | tr -d "\"\'" | sed -e 's/^image: //' -e "s/['\"]//g" | grep ":" | sort | uniq); do
    echo "  - sync image ${source}"
    container_image_sync "${source}"
  done
}

function kanod_image_in_value_sync() {
  echo "- sync of $1"
  for source in $(grep -o 'value: .*/.*:.*' "$1" | tr -d "\"\'" | sed -e 's/^value: //' -e "s/['\"]//g" | grep ":" | sort | uniq); do
    echo "  - sync image ${source}"
    container_image_sync "${source}"
  done
}

function container_image_sync (){
    source="${1}"
    # shellcheck disable=SC2001
    path=$(sed -e 's![^/]*[:.][^/]*/!!' <<< "$source")
    
    if [[ "$path" == redis* ]]; then 
        path="library/$path"
    fi
    target="${REGISTRY}/${path}"
    if [[ "${source}" == 'registry.gitlab.com/orange-opensource/kanod/'* ]]; then
        echo "- local image (do not sync)"
        return
    fi

    if [ "$(curl -sL --insecure -o /dev/null "${REGISTRY}/v2/${path/://manifests/}" -w '%{http_code}')" == 200 ]; then
        echo "- already synchronized"
        return
    fi
    # TODO: How do we know this is an official image ?
    echo "${source} => ${target}"
    if [ -n "${REGISTRY_MIRROR:-}" ]; then
        # If we use a registry mirror, we first try to find the image there
        # shellcheck disable=SC2001
        altimg="$REGISTRY_MIRROR/${path}"
        echo "skopeo with registry mirror: ${altimg} => ${target}"
        if skopeo copy --src-tls-verify=false --dest-tls-verify=false  "docker://${altimg}" "docker://${target}"; then
            return
        fi
    fi
    echo "skopeo : ${source} => ${target}"
    skopeo copy --dest-tls-verify=false  "docker://${source}" "docker://${target}"
}

echo "Sync images for cert manager"
kanod_image_sync build/cert-manager.yml
echo
echo "Sync images for stack"
kanod_image_sync build/stack.yml
echo
echo "Sync images for ingress"
kanod_image_sync build/kanod-ingress.yml
echo
echo "Sync components"
for entry in ironic bmoperator bm-crd capi capi-hosts hostclaim host-baremetal host-kubevirt nfs-provisioner clusterdef dashboard baremetalpool network-operator rke2 external brokerdef brokernet kamaji bmh-data kea ipam openstack rook rook-ceph; do
  echo "- ${entry}"
  kanod_image_sync "build/kn-${entry}.yml"
done
echo

# argocd version cmp and krm
echo "- argocd-cmp"
kanod_image_sync build/kn-argocd-cmp.yml
echo "- argocd-krm"
kanod_image_sync build/kn-argocd-krm.yml


AUTOSCALER_VERSION=$(grep "^AUTOSCALER_VERSION=" manifests/Makefile | cut -d "=" -f 2)
echo "Syncing kube-vip image version ${AUTOSCALER_VERSION}"
image="registry.k8s.io/autoscaling/cluster-autoscaler:${AUTOSCALER_VERSION}"
container_image_sync "${image}"

KUBE_VIP_VERSION=$(grep "^KUBE_VIP_VERSION=" manifests/Makefile | cut -d "=" -f 2)
echo "Syncing kube-vip image version ${KUBE_VIP_VERSION}"
image="ghcr.io/kube-vip/kube-vip:${KUBE_VIP_VERSION}"
container_image_sync "${image}"

echo "Sync kubevirt component"
kanod_image_sync "build/kn-kubevirt.yml"
kanod_image_in_value_sync "build/kn-kubevirt.yml"
echo
