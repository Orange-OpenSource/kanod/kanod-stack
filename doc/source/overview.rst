Overview
========
The Kanod stack is a set of manifests that specify a Kanod deployment. Although
they could be provisioned manually with kubectl and kustomize, the prefered
approach uses a dedicated operator.

The Kanod Operator simplifies the deployment of Kanod by proposing a centralized
resource to define the components to install. The operator takes care of the
necessary synchronization between the different components and publishes a
set of conditions representing the state of deployment of those components.
A given operator will deploy a set of consistent versions of the various controllers
needed.

The main components handled by the operator are:

* Cluster API
* Metal3
* ArgoCD and plugins for cluster definition
* Transient multi-tenant BareMetalHost with BareMetalHost broker and associated
  Network Isolation
* BringYourOwnHost (prototype)
* Hybrid and Multi-tenant clusters with HostClaims

.. warning::

    Note that HostClaims and the broker approach (with optionally BringYourOwnHost)
    are two incompatible approaches for solving the same goals.

There are also various auxiliary bricks that can be installed:

* Kamaji
* Kubevirt
* TPM based security
* Ingress based on nginx and MetalLB

Here is an example of a Kanod configuration:

.. code-block::yaml

    apiVersion: config.kanod.io/v1
    kind: Kanod
    metadata:
      name: kanod
      namespace: kanod
    spec:
      ingress:
        ip: 192.168.133.11
        name: lcm
        loadBalancerRange: 192.168.133.11-192.168.133.20
      ironic:
        interface: br0
      argocd:
        gitcerts:
        - host: git.service.kanod.home.arpa
          valueFrom:
            configMapName: config
            key: repo_ca
        baremetal:
          url: https://git.service.kanod.home.arpa/infra_admin/hosts.git
          credentials: git-admin
        projects:
          url: https://git.service.kanod.home.arpa/infra_admin/metaproject.git
          credentials: git-admin

By default CAPI is deployed. The BareMetalOperator will be deployed with Ironic
connected to bridge ``br0``. ArgoCD with the ClusterDef controller will be
configured for two repository: one for baremetal server definitions and the
other for cluster definitions. The credentials to access the git repositories
are stored in a secret (login and password under fields ``username`` and
``password`` respectively).

The Kanod operator also use a ConfigMap ``config`` in the same namespace.
This resource specifies several general purpose parameters

* ``http_proxy`` proxy for http connection if needed
* ``https_proxy`` proxy for https connection if needed
* ``no_proxy`` list of domains/hosts excluded from proxy
* ``registry`` Local registry
* ``repository`` Repository for OS image
* ``vault_url`` URL for vault when used with Vault
* ``vault_ca`` Certificate authority of Vault Deployment
* ``vault_role`` Role of the cluster when used with Vault
