#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -eu

basedir=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")
build="${basedir}/build"

url="$REPO_URL"
version="$STACK_VERSION"

groupId="kanod"
groupPath=$(echo "$groupId" | tr '.' '/')
artifactId="stack"

if ! curl --head --silent --fail "${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.yml"; then
  echo "Uploading stack : ${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.yml"

  if [ -f "${build}/stack.yml" ]; then
    # shellcheck disable=SC2086
    mvn ${MAVEN_CLI_OPTS} deploy:deploy-file ${MAVEN_OPTS} -DgroupId="${groupId}" -DartifactId="${artifactId}" \
      -Dversion="${version}" -Dtype=yml -Dfile="${build}/stack.yml" \
      -DrepositoryId=kanod -Durl="${url}" \
      -Dfiles="${build}/cert-manager.yml,${build}/applications.yml,${build}/tpm-registrar.yml,${build}/kanod-operator.yml" \
      -Dtypes=yml,yml,yml,yml -Dclassifiers=cm,apps,registrar,operator
  else
    echo "Problem during generation of stack"
  fi
else
  echo "stack already exists : ${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.yml"
fi
