package resources

import (
	_ "embed"
)

//go:embed cert-manager.yml
var CertManager []byte

//go:embed kn-ironic.yml
var Ironic []byte

//go:embed kn-bmoperator.yml
var BMO []byte

//go:embed kn-capi.yml
var Capi []byte

//go:embed kn-capi-hosts.yml
var CapiHosts []byte

//go:embed kn-host-kubevirt.yml
var CapiHostKubevirt []byte

//go:embed kn-kubevirt.yml
var Kubevirt []byte

//go:embed kn-nfs-provisioner.yml
var NfsProvisioner []byte

//go:embed kn-argocd-cmp.yml
var ArgocdCmp []byte

//go:embed kn-argocd-krm.yml
var ArgocdKrm []byte

//go:embed kn-clusterdef.yml
var Clusterdef []byte

//go:embed kn-dashboard.yml
var Dashboard []byte

//go:embed kn-rke2.yml
var Rke2 []byte

//go:embed kanod-ingress.yml
var Ingress []byte

//go:embed kn-app-baremetals.yml
var AppBaremetals []byte

//go:embed kn-app-projects.yml
var AppProjects []byte

//go:embed kn-app-network.yml
var AppNetwork []byte

//go:embed tpm-registrar.yml
var TpmRegistrar []byte

//go:embed kn-baremetalpool.yml
var Baremetalpool []byte

//go:embed kn-network-operator.yml
var NetworkOperator []byte

//go:embed kn-external.yml
var External []byte

//go:embed kn-brokerdef.yml
var Brokerdef []byte

//go:embed kn-brokernet.yml
var Brokernet []byte

//go:embed kn-kamaji.yml
var Kamaji []byte

//go:embed kn-bmh-data.yml
var BmhData []byte

//go:embed kn-kea.yml
var Kea []byte

//go:embed kn-host-baremetal.yml
var HostBaremetal []byte

//go:embed kn-hostclaim.yml
var HostClaim []byte

//go:embed kn-openstack.yml
var Openstack []byte

//go:embed kn-ipam.yml
var IpamStandalone []byte

//go:embed kn-rook.yml
var Rook []byte

//go:embed kn-rook-ceph.yml
var RookCeph []byte

//go:embed kn-bm-crd.yml
var BaremetalCrd []byte
