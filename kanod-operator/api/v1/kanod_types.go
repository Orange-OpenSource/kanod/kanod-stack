/*
Copyright 2025.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// KanodSpec defines the desired state of Kanod
type KanodSpec struct {
	// Name of the configmap containing the core configuration. Defaults to
	// config
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=config
	ConfigName string `json:"configName,omitempty"`

	// Ironic is the Ironic configuration
	Ironic *Ironic `json:"ironic,omitempty"`
	// Capi is a boolean to optionally disable CAPI
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	Capi bool `json:"capi"`
	// CapiHosts enables the support of hosts in the stack
	CapiHosts bool `json:"capiHosts,omitempty"`
	// CapiHostKubevirt enables the support of kubevirt hosts in the stack
	CapiHostKubevirt bool `json:"capiHostKubevirt,omitempty"`
	// Kubevirt enables the support of kubevirt  in the stack
	Kubevirt *KubevirtConfig `json:"kubevirt,omitempty"`
	// CapiHostsOpenstack enables the support of hosts targeting Openstack
	// VM in the stack
	CapiHostsOpenstack bool `json:"capiHostsOpenstack,omitempty"`
	// NoDhcp enables the support of provisioning servers without DHCP server
	NoDhcp bool `json:"noDhcp,omitempty"`
	// Rke2 is a boolean to optionally disable Capi Rke2 support
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=false
	Rke2 bool `json:"rke2"`
	// CertManager is a boolean to optionally disable Cert-Manager (if provided elsewhere)
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	CertManager bool `json:"certManager"`
	// ExternalSecret is a boolean to optionally disable externalSecret when Vault is used.
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	ExternalSecrets bool `json:"externalSecret"`
	// BrokerDef is the configuration of a redfish Broker
	BrokerDef *BrokerDef `json:"brokerDef,omitempty"`
	// BrokerNet is the configuration of a network broker
	BrokerNet *BrokerNet `json:"brokerNet,omitempty"`
	// ArgoCD is the configuration of argocd projects
	ArgoCD *ArgoCD `json:"argocd,omitempty"`
	// Ingress
	Ingress *Ingress `json:"ingress,omitempty"`
	// Broker contains the ip of the broker Redfish
	Broker *Broker `json:"broker,omitempty"`
	// Kamaji is a boolean to optionally enable Capi Kamaji support
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=false
	Kamaji bool `json:"kamaji"`
}

const (
	CertManagerReadyCondition     = "CertManagerReady"
	KanodStackReadyCondition      = "KanodStackReady"
	BaremetalReadyCondition       = "BaremetalReady"
	ClusterApiReadyCondition      = "ClusterApiReady"
	KubevirtReadyCondition        = "KubevirtReady"
	NfsProvisionerReadyCondition  = "NfsProvisionerReady"
	HostClaimReadyCondition       = "HostClaimReady"
	HostKubevirtReadyCondition    = "HostKubevirtReady"
	HostBaremetalReadyCondition   = "HostBaremetalReady"
	HostOpenstackReadyCondition   = "HostOpenstackReady"
	OpenstackReadyCondition       = "OpenstackReady"
	GitopsReadyCondition          = "GitopsReady"
	ClusterdefReadyCondition      = "ClusterdefReady"
	RegistrarReadyCondition       = "RegistrarReady"
	IngressReadyCondition         = "IngressReady"
	ExternalReadyCondition        = "ExternalReady"
	BaremetalpoolReadyCondition   = "BaremetalpoolReady"
	NetworkOperatorReadyCondition = "NetworkOperatorReady"
	BmhDataReadyCondition         = "BmhDataReady"
	KeaReadyCondition             = "KeaReady"
	RookReadyCondition            = "RookReady"
	WaitingForComponentReason     = "WaitingForComponent"
	ComponentDeployedReason       = "ComponentDeployed"
	InternalFailureReason         = "InternalFailure"
	Rke2ReadyCondition            = "Rke2Ready"
)

// KanodStatus defines the observed state of Kanod
type KanodStatus struct {
	// conditions describe the state of the built image
	// +patchMergeKey=type
	// +patchStrategy=merge
	// +listType=map
	// +listMapKey=type
	// +optional
	Conditions []metav1.Condition `json:"conditions,omitempty" patchMergeKey:"type" patchStrategy:"merge"`
}

type Ironic struct {
	// Include BareMetalOperator (default to true)
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	Bmo bool `json:"bmo"`
	// Do not include Ironic (default to false)
	// +optional
	// +kubebuilder:validation:Optional
	NoIronic bool `json:"noIronic,omitempty"`
	// Whether to enable the use of TPM for server authentication
	Tpm *Tpm `json:"tpm,omitempty"`
	// Interface used by Ironic to contact servers
	Interface string `json:"interface"`
	// Ironic IP on the interface with servers
	IP string `json:"ip,omitempty"`
	// External IP used by BareMetalHosts. Can be used to proxify Ironic
	ExternalIP string `json:"externalIp,omitempty"`
	// Ironic Python Agent kernel URL
	IpaKernel string `json:"ipaKernel,omitempty"`
	// Ironic Python Agent ram disk URL
	IpaRamdisk string `json:"ipaRamdisk,omitempty"`
	// additional parameters for the IPA kernel
	IpaKernelParams string `json:"ipaKernelParams,omitempty"`
}

type Ingress struct {
	// Ip used by the ingress (load-balancer)
	IP string `json:"ip"`
	// external name
	Name string `json:"name"`
	// Load balancer range
	LBRange string `json:"loadBalancerRange"`
	// Key for the certificate
	Key *StringValue `json:"key,omitempty"`
	// Certificate published by the ingress
	Certificate *StringValue `json:"certificate,omitempty"`
	// Whether to use MetalLB or not
	MetalLB bool `json:"metallb,omitempty"`
}

type Broker struct {
	// IP of the broker to propagate with CoreDNS inside the cluster
	// This option can usually be left empty as this can be handled by
	// a DNS server outside Kanod and there is a single domain advocated.
	IP string `json:"ip,omitempty"`
}

type BrokerDef struct {
	// Credentials is the name of secret hosting admin credenitals
	Credentials string `json:"credentials"`
	// RedfishDomain is the domain used for exposing BMC of servers
	RedfishDomain string `json:"redfishDomain,omitempty"`
	// DelegateAuth controls the kind of proxy activated (real proxy or delegation to user on BMC)
	DelegatedAuth bool `json:"delegatedAuth,omitempty"`
}

type BrokerNet struct {
	// Credentials is the name of secret hosting admin credenitals
	Credentials string `json:"credentials"`
}
type Dhcp struct {
	Enabled bool `json:"enabled"`
}

type Tpm struct {
	// AuthUrl is the URL of endpoint to connect to for authentication
	AuthUrl string `json:"authUrl"`
	// AuthCa is the certificate used for validating the authentication URL
	AuthCa *StringValue `json:"authCa"`
	// Registrar is the endpoint for the registration part.
	Registrar *Endpoint `json:"registrar,omitempty"`
}

type Endpoint struct {
	// Ip part of the endpoint
	IP string `json:"ip"`
	// TCP port used by the endpoint
	Port int `json:"port"`
}

type ArgoCD struct {
	// GitCerts is a list of certificates for git servers accessed by ArgoCD
	GitCerts []GitCertificates `json:"gitcerts,omitempty"`
	// Baremetal is a project description for baremetal hosts
	Baremetal *ArgoProject `json:"baremetal,omitempty"`
	// Baremetal is a project description for cluster definitions
	Projects *ArgoProject `json:"projects,omitempty"`
	// Baremetal is a project description for network infrastructure specification
	Network *ArgoProject `json:"network,omitempty"`
	// ClusterDef controls the activation of cluster def operator
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	ClusterDef bool `json:"clusterdef"`
	// +optional
	// +kubebuilder:validation:Optional
	// +kubebuilder:default:=true
	// ClusterDef controls the activation of the LCM dashboard
	Dashboard bool `json:"dashboard"`
}

type GitCertificates struct {
	// Host name of the Git server
	Host        string `json:"host"`
	StringValue `json:",inline"`
}

type StringValue struct {
	// Explicit value associated as a string
	Value string `json:"value,omitempty"`
	// Pointer to a key in a Kubernetes resource (configmap or secret)
	ValueFrom *KeyRef `json:"valueFrom,omitempty"`
}

type KeyRef struct {
	// Name of the configmap (exclusive with secret)
	ConfigMapName string `json:"configMapName,omitempty"`
	// Name of the secret (exclusive with configmap)
	SecretName string `json:"secretName,omitempty"`
	// Name of the key
	Key string `json:"key,omitempty"`
}

type ArgoProject struct {
	// URL of ArgoCD project
	Url string `json:"url"`
	// Name of a secret in same namespace holding the credentials
	Credentials string `json:"credentials"`
}

type KubevirtConfig struct {
	// is kubevirt enabled
	Enabled bool `json:"enabled"`
	// NfsProvisioner config
	NfsProvisioner *NfsProvisionerConfig `json:"nfsProvisioner,omitempty"`
	// is Rook enabled (limited to the operator in default config)
	Rook bool `json:"rook"`
}

type NfsProvisionerConfig struct {
	// Nfs server Address
	NfsServer string `json:"nfsServer"`
	// Nfs server path
	NfsPath string `json:"nfsPath"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// Kanod is the Schema for the kanods API
type Kanod struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   KanodSpec   `json:"spec,omitempty"`
	Status KanodStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// KanodList contains a list of Kanod.
type KanodList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Kanod `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Kanod{}, &KanodList{})
}
