package controller

import (
	"context"

	configv1 "gitlab.com/Orange-OpenSource/kanod/kanod-stack/kanod-operator/api/v1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

func (r *KanodReconciler) checkPodsReady(ctx context.Context, namespaces []string) bool {
	for _, namespace := range namespaces {
		pods := &corev1.PodList{}
		if err := r.Client.List(ctx, pods, &client.ListOptions{Namespace: namespace}); err != nil {
			r.Log.Error(err, "Error while listing pods for readyness")
			return false
		}
		for _, pod := range pods.Items {
			if pod.Status.Phase != corev1.PodRunning && pod.Status.Phase != corev1.PodSucceeded {
				return false
			}
		}
	}
	return true
}

func (r *KanodReconciler) checkCertmanagerEndpoints(ctx context.Context) bool {
	endpoint := &corev1.Endpoints{}
EndpointLoop:
	for _, name := range []string{"cert-manager", "cert-manager-webhook"} {
		key := client.ObjectKey{
			Name:      name,
			Namespace: "cert-manager",
		}
		err := r.Client.Get(ctx, key, endpoint)
		if err != nil {
			return false
		}
		for _, subset := range endpoint.Subsets {
			for _, address := range subset.Addresses {
				if address.IP != "" {
					r.Log.Info("Found ip for endpoint", "endpoints", name, "IP", address)
					continue EndpointLoop
				}
			}
		}
		// no IP for that endpoints
		return false
	}
	return true
}

type WaitFor struct {
	ConditionType string
	Namespaces    []string
}

func (r *KanodReconciler) checkDeploymentsAvailable(
	ctx context.Context,
	kanod *configv1.Kanod,
	waitfors []WaitFor,
) bool {
	globalStatus := true
WaitForLoop:
	for _, waitfor := range waitfors {
		for _, namespace := range waitfor.Namespaces {
			deployments := &appsv1.DeploymentList{}
			if err := r.Client.List(ctx, deployments, &client.ListOptions{Namespace: namespace}); err != nil {
				r.Log.Error(err, "Error while listing pods for readyness")
				globalStatus = false
				continue WaitForLoop
			}
		DeploymentLoop:
			for _, dep := range deployments.Items {
				for _, condition := range dep.Status.Conditions {
					if condition.Type == appsv1.DeploymentAvailable && condition.Status == corev1.ConditionTrue {
						// This one is fine, go on to next deployment
						continue DeploymentLoop
					}
				}
				// We found an unavailable deployment
				globalStatus = false
				continue WaitForLoop
			}
		}
		meta.SetStatusCondition(
			&kanod.Status.Conditions,
			metav1.Condition{
				Type:   waitfor.ConditionType,
				Status: metav1.ConditionTrue,
				Reason: configv1.ComponentDeployedReason,
			})
	}
	return globalStatus
}
