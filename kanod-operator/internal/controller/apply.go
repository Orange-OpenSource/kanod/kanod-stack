package controller

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/drone/envsubst"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/yaml"
	"k8s.io/client-go/discovery/cached/memory"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/restmapper"
)

func (r *KanodReconciler) ApplyFile(ctx context.Context, client *http.Client, env *Env, url string) error {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		r.Log.Error(err, "cannot create resource request", "url", url)
		return err
	}

	resp, err := client.Do(req)
	if err != nil {
		r.Log.Error(err, "cannot read resource", "url", url)
		return err
	}
	if resp.StatusCode != 200 {
		err = fmt.Errorf("bad status code: %d", resp.StatusCode)
		r.Log.Error(err, "read resource failed")
		return err
	}

	defer func() {
		if err := resp.Body.Close(); err != nil {
			r.Log.Error(err, "error in resp.Body.Close()")
		}
	}()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		r.Log.Error(err, "cannot extract resource body", "url", url)
		return err
	}
	return r.ApplyData(ctx, env, url, data)
}

func (r *KanodReconciler) GetUnstructured(
	env *Env,
	resourceName string,
	data []byte,
) ([]unstructured.Unstructured, error) {
	buf, err := envsubst.Eval(string(data), env.Getenv)
	if len(env.Errors) > 0 {
		r.Log.Info("Missing substitution for variables", "variables", strings.Join(env.Errors, ", "))
		env.Errors = nil
	}
	if err != nil {
		r.Log.Error(err, "envsubst failed", "origin", resourceName)
		return nil, err
	}
	resources := strings.Split(buf, "\n---\n")
	result := make([]unstructured.Unstructured, len(resources))
	for pos, resource := range resources {
		u := &result[pos]
		if err := yaml.Unmarshal([]byte(resource), &u); err != nil {
			r.Log.Error(err, "cannot unmarshal resource", "origin", resourceName, "pos", pos)
			return nil, err
		}
	}
	return result, nil
}

func (r *KanodReconciler) ApplyUnstructured(ctx context.Context,
	resourceName string,
	resources []unstructured.Unstructured,
) error {
	dynclient, err := dynamic.NewForConfig(r.RestConfig)
	if err != nil {
		r.Log.Error(err, "cannot get dynclient")
		return err
	}
	clientset, err := kubernetes.NewForConfig(r.RestConfig)
	if err != nil {
		r.Log.Error(err, "cannot get clientset")
		return err
	}
	discoveryClient := clientset.DiscoveryClient
	cachedDiscoveryClient := memory.NewMemCacheClient(discoveryClient)
	mapper := restmapper.NewDeferredDiscoveryRESTMapper(cachedDiscoveryClient)

	for _, u := range resources {
		if u.Object == nil {
			r.Log.Info("Found an empty object. Skipping.")
			continue
		}
		gvk := u.GroupVersionKind()

		mapping, err := mapper.RESTMapping(gvk.GroupKind(), gvk.Version)
		if err != nil {
			r.Log.Error(err, "cannot get the gvr")
			return err
		}
		name := u.GetName()
		namespace := u.GetNamespace()
		if strings.HasPrefix(gvk.Kind, "ClusterRole") {
			namespace = ""
		}
		log := r.Log.WithValues(
			"resource", resourceName, "name", name, "namespace",
			namespace, "group", gvk.Group, "version", gvk.Version,
			"kind", gvk.Kind)
		dync := dynclient.Resource(mapping.Resource).Namespace(namespace)
		if _, err := dync.Get(ctx, name, metav1.GetOptions{}); k8serrors.IsNotFound(err) {
			log.Info("Creating resource")
			_, err = dync.Create(ctx, &u, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "cannot create the object dynamically")
				return err
			}
		} else if err != nil {
			log.Error(err, "failure during dynamic get")
		}
	}
	return nil
}

func (r *KanodReconciler) ApplyData(ctx context.Context, env *Env, resourceName string, data []byte) error {
	resources, err := r.GetUnstructured(env, resourceName, data)
	if err != nil {
		return err
	}
	return r.ApplyUnstructured(ctx, resourceName, resources)
}

func (r *KanodReconciler) AddApplicationPlugin(ctx context.Context, name string) error {
	dynclient, err := dynamic.NewForConfig(r.RestConfig)
	if err != nil {
		r.Log.Error(err, "cannot get dynclient")
		return err
	}
	resource := schema.GroupVersionResource{
		Group:    "argoproj.io",
		Version:  "v1alpha1",
		Resource: "applications",
	}
	dync := dynclient.Resource(resource).Namespace("argocd")
	patch := `{"spec": {"source": {"plugin": {"env": [{"name": "PLUGIN_MODE", "value": "vault-kustomize" }]}}}}`
	_, err = dync.Patch(ctx, name, types.MergePatchType, []byte(patch), metav1.PatchOptions{})
	if err != nil {
		r.Log.Error(err, "Cannot inject vault-kustomize-plugin in application", "application", name)
	}
	return nil
}
