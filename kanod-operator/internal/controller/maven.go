package controller

import (
	"fmt"

	xmlpath "gopkg.in/xmlpath.v2"
)

const (
	SUBPATH = "/repository/kanod/kanod"
)

func (r *KanodReconciler) MavenRelease(artifact string) (string, error) {
	baseUrl := fmt.Sprintf("%s%s/%s", r.Config.RepoUrl, SUBPATH, artifact)
	if r.RepoClient == nil {
		r.RepoClient = r.NewHttpClient()
	}
	mavenInfo := fmt.Sprintf("%s/maven-metadata.xml", baseUrl)
	resp, err := r.RepoClient.Get(mavenInfo)
	if err != nil {
		return "", err
	}
	defer func() {
		if err := resp.Body.Close(); err != nil {
			r.Log.Error(err, "error in resp.Body.Close()")
		}
	}()

	node, err := xmlpath.Parse(resp.Body)
	if err != nil {
		return "", err
	}
	release, ok := xmlpath.MustCompile("/metadata/versioning/release").String(node)
	if !ok {
		return "", fmt.Errorf("no release found")
	}
	return release, nil
}

func (r *KanodReconciler) ArtifactUrl(artifact string, release string, classifier string, extension string) string {
	if classifier != "" {
		return fmt.Sprintf(
			"%s%s/%s/%s/%s-%s-%s.%s",
			r.Config.RepoUrl, SUBPATH,
			artifact, release, artifact, release, classifier, extension,
		)
	}
	return fmt.Sprintf(
		"%s%s/%s/%s/%s-%s.%s",
		r.Config.RepoUrl, SUBPATH,
		artifact, release, artifact, release, extension,
	)
}
