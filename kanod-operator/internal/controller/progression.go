package controller

import (
	"context"
	"fmt"
	"time"

	kanodv1 "gitlab.com/Orange-OpenSource/kanod/kanod-stack/kanod-operator/api/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	ANNOTATION = "operator.kanod.io/progression"
	TIMEOUT    = 3600
)

func (r *KanodReconciler) getProgression(kanod *kanodv1.Kanod) (State, int64, error) {
	annotation, present := kanod.Annotations[ANNOTATION]
	now := time.Now().Unix()
	if !present {
		r.Log.Info("Did not find annotation")
		return Initial, now, nil
	}
	var step State
	var recDate int64
	_, err := fmt.Sscanf(annotation, "%d/%d", &step, &recDate)
	if err != nil {
		r.Log.Error(err, fmt.Sprintf("Cannot decode annotation: %s", annotation))
		return step, recDate, err
	}
	if step > Initial && recDate+TIMEOUT < now {
		return step, recDate, fmt.Errorf("timeout exceeded during step %d", step)
	}
	return step, recDate, nil
}

func (r *KanodReconciler) setProgression(ctx context.Context, kanod *kanodv1.Kanod, step State) error {
	now := time.Now().Unix()
	patch := fmt.Sprintf(`{"metadata":{"annotations":{"%s": "%d/%d"}}}`, ANNOTATION, step, now)
	err := r.Client.Patch(
		ctx,
		&kanodv1.Kanod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      kanod.Name,
				Namespace: kanod.Namespace,
			},
		},
		client.RawPatch(types.MergePatchType, []byte(patch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot patch kanod configuration.")
	} else {
		r.Log.Info("Phase completed", "phase", step)
	}
	return err
}

func (r *KanodReconciler) SetAnnotation(
	ctx context.Context,
	kanod *kanodv1.Kanod,
	annotationName string,
	annotationValue string,
) error {
	patch := fmt.Sprintf(`{"metadata":{"annotations":{"%s": "%s"}}}`, annotationName, annotationValue)
	err := r.Client.Patch(
		ctx,
		&kanodv1.Kanod{
			ObjectMeta: metav1.ObjectMeta{
				Name:      kanod.Name,
				Namespace: kanod.Namespace,
			},
		},
		client.RawPatch(types.MergePatchType, []byte(patch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot patch kanod annotation.")
	} else {
		r.Log.Info("kanod annotated", "annotationName", annotationName, "annotationValue", annotationValue)
	}
	return err
}
