/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"errors"
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/go-logr/logr"
	"golang.org/x/crypto/bcrypt"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	configv1 "gitlab.com/Orange-OpenSource/kanod/kanod-stack/kanod-operator/api/v1"
	"gitlab.com/Orange-OpenSource/kanod/kanod-stack/kanod-operator/resources"
)

const (
	RETRY_DELAY        = 5
	IPA_ARTIFACT       = "ironic-python-agent"
	IPA_GLEAN_ARTIFACT = "ironic-python-agent-glean"
)

type State = int

const (
	Initial State = iota
	ConfigureCoreDns
	CertManagerDeploy
	CertManagerWaitEndpoint
	CertManagerWaitStable
	IronicDeploy
	BmoDeploy
	BaremetalCrdDeploy
	CapiDeploy
	IpamDeploy
	HostBaremetalDeploy
	ArgoCDDeploy
	ClusterDefDeploy
	IngressDeploy
	TpmRegistrarDeploy
	BaremetalpoolDeploy
	NetworkOperatorDeploy
	BmhDataDeploy
	KeaDeploy
	CapiRke2Deploy
	CapiKamajiDeploy
	ExternalSecretsDeploy
	BrokerdefDeploy
	BrokernetDeploy
	OpenstackDeploy
	HostClaimDeploy
	DashboardDeploy
	GitCertsDeploy
	ConfigureIronicDeployment
	KubevirtDeploy
	NfsProvisionerDeploy
	RookDeploy
	RookCephDeploy
	HostKubevirtDeploy
	HostOpenstackDeploy
	StackWaitStable
	ApplicationDeploy
	ApplicationWaitStable
	Final
)

// KanodReconciler reconciles a Kanod object
type KanodReconciler struct {
	client.Client
	Log        logr.Logger
	RestConfig *rest.Config
	Config     *KanodConfig
	RepoClient *http.Client
}

// KanodConfig is the static part of the operator configuration retrieved
// mainly from the configMap in kanod.
type KanodConfig struct {
	RepoUrl    string
	RepoCa     string
	Registry   string
	HttpProxy  string
	HttpsProxy string
	NoProxy    string
	VaultUrl   string
	VaultCa    string
	VaultRole  string
}

func (r *KanodReconciler) NewKanodConfig(
	ctx context.Context,
	name string,
	namespace string,
) (*KanodConfig, error) {
	config := &corev1.ConfigMap{}
	cmapMeta := client.ObjectKey{
		Name:      name,
		Namespace: namespace,
	}
	err := r.Client.Get(ctx, cmapMeta, config)
	if err != nil {
		r.Log.Error(err, "Cannot find the base config", "configName", name)
		return nil, err
	}
	repository, ok := config.Data["repository"]
	if !ok {
		r.Log.Error(err, "Cannot proceed without repo", "configName", name)
		return nil, err
	}
	registry, ok := config.Data["registry"]
	if !ok {
		r.Log.Error(err, "Cannot proceed without registry", "configName", name)
		return nil, err
	}
	kanodConfig := KanodConfig{
		RepoUrl:    repository,
		RepoCa:     config.Data["repo_ca"],
		Registry:   registry,
		HttpProxy:  config.Data["http_proxy"],
		HttpsProxy: config.Data["https_proxy"],
		NoProxy:    config.Data["no_proxy"],
		VaultUrl:   config.Data["vault_url"],
		VaultCa:    config.Data["vault_ca"],
		VaultRole:  config.Data["vault_role"],
	}
	return &kanodConfig, nil
}

func (r *KanodReconciler) NewHttpClient() *http.Client {
	pool := x509.NewCertPool()
	if r.Config.RepoCa != "" {
		pool.AppendCertsFromPEM([]byte(r.Config.RepoCa))
	}
	tlsConfig := &tls.Config{
		RootCAs: pool,
	}
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	return &http.Client{Transport: transport}
}

func b64(s string) string {
	return base64.StdEncoding.EncodeToString([]byte(s))
}

type Env struct {
	Env    map[string]string
	Errors []string
}

func (e *Env) Getenv(key string) string {
	v, ok := e.Env[key]
	if !ok {
		e.Errors = append(e.Errors, key)
	}
	return v
}

func (e *Env) Setenv(key string, val string) {
	e.Env[key] = val
}

// +kubebuilder:rbac:groups=config.kanod.io,resources=kanods,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=config.kanod.io,resources=kanods/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=config.kanod.io,resources=kanods/finalizers,verbs=update
// +kubebuilder:rbac:groups="",resources="configmaps",verbs=get;list;create;update;watch
// +kubebuilder:rbac:groups="apps",resources="deployment",verbs=get;list;update;patch
func (r *KanodReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	var kanod configv1.Kanod
	if err := r.Get(ctx, req.NamespacedName, &kanod); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	log := r.Log.WithValues("name", kanod.Name, "namespace", kanod.Namespace)
	if r.Config == nil {
		var err error
		r.Config, err = r.NewKanodConfig(
			ctx, kanod.Spec.ConfigName, kanod.Namespace)
		if err != nil {
			return ctrl.Result{}, err
		}
	}
	env := &Env{Env: make(map[string]string)}
	env.Setenv("NEXUS_REGISTRY", r.Config.Registry)
	env.Setenv("NEXUS", r.Config.RepoUrl)
	if r.Config.RepoCa != "" {
		env.Setenv("BASE64_REPO_CA", base64.StdEncoding.EncodeToString([]byte(r.Config.RepoCa)))
	}
	env.Setenv("MGT_HTTP_PROXY", r.Config.HttpProxy)
	env.Setenv("MGT_HTTPS_PROXY", r.Config.HttpsProxy)
	env.Setenv("MGT_NO_PROXY", r.Config.NoProxy)
	env.Setenv("VAULT_URL", r.Config.VaultUrl)
	env.Setenv("VAULT_CA_B64", b64(r.Config.VaultCa))
	env.Setenv("VAULT_K8S", r.Config.VaultRole)

	// TODO: support for TPM.

	ironic := kanod.Spec.Ironic
	if ironic != nil {
		env.Setenv("DISABLE_HOST_BAREMETAL", "false")
		env.Setenv("IRONIC_IP", ironic.IP)
		env.Setenv("IRONIC_EXTERNAL_IP", ironic.ExternalIP)
		if ironic.IpaKernel != "" {
			env.Setenv("IPA_KERNEL_URL", ironic.IpaKernel)
			env.Setenv("IPA_RAMDISK_URL", ironic.IpaRamdisk)
		} else {
			ipa_artifact := IPA_ARTIFACT
			if kanod.Spec.NoDhcp {
				ipa_artifact = IPA_GLEAN_ARTIFACT
			}
			release, err := r.MavenRelease(ipa_artifact)
			if err != nil {
				log.Error(err, "cannot get IPA Url")
				return ctrl.Result{}, err
			}
			env.Setenv("IPA_KERNEL_URL", r.ArtifactUrl(ipa_artifact, release, "kernel", "kernel"))
			env.Setenv("IPA_RAMDISK_URL", r.ArtifactUrl(ipa_artifact, release, "initramfs", "initramfs"))
		}
		env.Setenv("IRONIC_KERNEL_PARAMS", ironic.IpaKernelParams)
		env.Setenv("PXE_ITF", ironic.Interface)
		if ironic.Tpm != nil {
			authCa, err := r.GetValue(ctx, ironic.Tpm.AuthCa, kanod.Namespace)
			if err != nil {
				log.Error(err, "cannot get TPM CA")
				return ctrl.Result{}, err
			}
			env.Setenv("TPM_AUTH_CA_B64", b64(authCa))
			env.Setenv("TPM_AUTH_URL", ironic.Tpm.AuthUrl)
		} else {
			env.Setenv("TPM_AUTH_CA_B64", "")
			env.Setenv("TPM_AUTH_URL", "")
		}
	} else {
		env.Setenv("DISABLE_HOST_BAREMETAL", "true")
	}

	ingress := kanod.Spec.Ingress
	if ingress != nil {
		env.Setenv("INGRESS_IP", ingress.IP)
		env.Setenv("INGRESS_NAME", ingress.Name)
		if ingress.Key != nil {
			value, _ := r.GetValue(ctx, ingress.Key, kanod.Namespace)
			env.Setenv("INGRESS_KEY_B64", b64(value))
			value, _ = r.GetValue(ctx, ingress.Certificate, kanod.Namespace)
			env.Setenv("INGRESS_CERT_B64", b64(value))
			env.Setenv("INGRESS_SECRET", "ingress-external-cert")
		} else {
			env.Setenv("INGRESS_KEY_B64", "")
			env.Setenv("INGRESS_CERT_B64", "")
			env.Setenv("INGRESS_SECRET", "ingress-cert")
		}
		env.Setenv("LB_RANGE", ingress.LBRange)
	}

	argocd := kanod.Spec.ArgoCD

	step, recDate, err := r.getProgression(&kanod)
	if err != nil {
		log.Error(err, "Cannot get progression annotation")
		if err2 := r.setProgression(ctx, &kanod, 0); err2 != nil {
			log.Error(err2, "Cannot set progression annotation")
			err = errors.Join(err, err2)
			return ctrl.Result{}, err
		}
		return ctrl.Result{}, err
	}

	broker := kanod.Spec.Broker

	switch step {
	case Initial:
		if !r.checkPodsReady(ctx, []string{"kube-system"}) {
			return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
		}
		if err := r.setProgression(ctx, &kanod, ConfigureCoreDns); err != nil {
			log.Error(err, "Cannot set annotation for coredns configuration")
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case ConfigureCoreDns:
		if broker != nil && broker.IP != "" {
			log.Info("Configure coredns for broker")
			if err := r.configureCoreDns(ctx, log, broker.IP); err != nil {
				return ctrl.Result{}, err
			}
		}
		if err := r.setProgression(ctx, &kanod, CertManagerDeploy); err != nil {
			log.Error(err, "Cannot set annotation for cert-manager apply")
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case CertManagerDeploy:
		return r.applyComponent(
			kanod.Spec.CertManager,
			ctx, env, &kanod, "cert-manager",
			resources.CertManager,
			configv1.CertManagerReadyCondition,
			CertManagerWaitEndpoint)
	case CertManagerWaitEndpoint:
		if r.checkCertmanagerEndpoints(ctx) {
			log.Info("Cert manager is ready")
			meta.SetStatusCondition(
				&kanod.Status.Conditions,
				metav1.Condition{
					Type:   configv1.CertManagerReadyCondition,
					Status: metav1.ConditionTrue,
					Reason: configv1.ComponentDeployedReason,
				})
			if err := r.updateKanodStatus(&kanod); err != nil {
				return ctrl.Result{}, err
			}
			if err := r.setProgression(ctx, &kanod, CertManagerWaitStable); err != nil {
				log.Error(err, "Cannot set annotation for cert-manager wait")
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case CertManagerWaitStable:
		if recDate+RETRY_DELAY*4 < time.Now().Unix() {
			log.Info("Additional delay cert manager")
			return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
		}
		if err := r.setProgression(ctx, &kanod, IronicDeploy); err != nil {
			log.Error(err, "Cannot set annotation for requeue delay")
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case IronicDeploy:
		meta.SetStatusCondition(
			&kanod.Status.Conditions,
			metav1.Condition{
				Type:   configv1.KanodStackReadyCondition,
				Status: metav1.ConditionFalse,
				Reason: configv1.WaitingForComponentReason,
			})
		if err := r.updateKanodStatus(&kanod); err != nil {
			return ctrl.Result{}, err
		}
		return r.applyComponent(
			kanod.Spec.Ironic != nil && !kanod.Spec.Ironic.NoIronic,
			ctx, env, &kanod, "ironic", resources.Ironic, configv1.BaremetalReadyCondition, BmoDeploy)
	case BmoDeploy:
		return r.applyComponent(
			kanod.Spec.Ironic != nil && kanod.Spec.Ironic.Bmo,
			ctx, env, &kanod, "baremetal-operator", resources.BMO, "", BaremetalCrdDeploy)
	case BaremetalCrdDeploy:
		return r.applyComponent(
			kanod.Spec.Capi && (kanod.Spec.Ironic == nil || !kanod.Spec.Ironic.Bmo),
			ctx, env, &kanod, "baremetal-crd", resources.BaremetalCrd, "", CapiDeploy)
	case CapiDeploy:
		capiResource := resources.Capi
		if kanod.Spec.CapiHosts {
			capiResource = resources.CapiHosts
		}
		return r.applyComponent(
			kanod.Spec.Capi,
			ctx, env, &kanod, "capi", capiResource, configv1.ClusterApiReadyCondition, IpamDeploy)
	case IpamDeploy:
		return r.applyComponent(
			!kanod.Spec.Capi && kanod.Spec.Ironic != nil,
			ctx, env, &kanod, "metal3-ipam", resources.IpamStandalone, "", HostBaremetalDeploy)
	case HostBaremetalDeploy:
		return r.applyComponent(
			kanod.Spec.CapiHosts && kanod.Spec.Ironic != nil,
			ctx, env, &kanod, "host-baremetal", resources.HostBaremetal, configv1.HostBaremetalReadyCondition, ArgoCDDeploy)
	case ArgoCDDeploy:
		name := "argocd-cmp"
		rsrc := resources.ArgocdCmp
		if kanod.Spec.ArgoCD != nil {
			if kanod.Spec.ArgoCD.ClusterDef {
				log.Info("Deploying Argocd with cmp mode")
			} else {
				log.Info("Deploying Argocd with krm mode")
				name = "argocd-krm"
				rsrc = resources.ArgocdKrm
			}
		}
		return r.applyComponent(
			kanod.Spec.ArgoCD != nil,
			ctx, env, &kanod, name, rsrc, configv1.GitopsReadyCondition, ClusterDefDeploy)

	case ClusterDefDeploy:
		return r.applyComponent(
			kanod.Spec.ArgoCD != nil && kanod.Spec.ArgoCD.ClusterDef,
			ctx, env, &kanod, "clusterdef", resources.Clusterdef, configv1.ClusterdefReadyCondition, IngressDeploy)
	case IngressDeploy:
		return r.applyComponent(
			kanod.Spec.Ingress != nil,
			ctx, env, &kanod, "ingress", resources.Ingress, configv1.IngressReadyCondition, TpmRegistrarDeploy)
	case TpmRegistrarDeploy:
		if kanod.Spec.Ironic != nil && kanod.Spec.Ironic.Tpm != nil {
			registrar := kanod.Spec.Ironic.Tpm.Registrar
			if registrar != nil {
				env.Setenv("REGISTRAR_IP", registrar.IP)
				env.Setenv("REGISTRAR_PORT", strconv.Itoa(registrar.Port))
			} else {
				if kanod.Spec.Ingress != nil {
					env.Setenv("REGISTRAR_IP", kanod.Spec.Ingress.IP)
					env.Setenv("REGISTRAR_PORT", "443")
				} else {
					env.Setenv("REGISTRAR_IP", kanod.Spec.Ironic.IP)
					env.Setenv("REGISTRAR_PORT", "8443")
				}
			}
			env.Setenv("REGISTRAR_CA_B64", b64(r.Config.VaultCa))
		}
		return r.applyComponent(
			kanod.Spec.Ironic != nil && kanod.Spec.Ironic.Tpm != nil,
			ctx, env, &kanod, "registrar", resources.TpmRegistrar,
			configv1.RegistrarReadyCondition, BaremetalpoolDeploy)
	case BaremetalpoolDeploy:
		return r.applyComponent(
			kanod.Spec.Broker != nil,
			ctx, env, &kanod, "baremetalpool", resources.Baremetalpool,
			configv1.BaremetalpoolReadyCondition, NetworkOperatorDeploy)
	case NetworkOperatorDeploy:
		return r.applyComponent(
			kanod.Spec.Broker != nil,
			ctx, env, &kanod, "network-operator", resources.NetworkOperator,
			configv1.NetworkOperatorReadyCondition, BmhDataDeploy)
	case BmhDataDeploy:
		// Could be useful to be able to disable it. But this is not linked strictly to IPAM.
		return r.applyComponent(
			kanod.Spec.Ironic != nil,
			ctx, env, &kanod, "bmh-data", resources.BmhData, configv1.BmhDataReadyCondition, KeaDeploy)
	case KeaDeploy:
		// Could be useful to be able to disable it. But this is not linked strictly to IPAM.
		return r.applyComponent(
			kanod.Spec.Ironic != nil && kanod.Spec.Capi,
			ctx, env, &kanod, "kea", resources.Kea, configv1.KeaReadyCondition, CapiRke2Deploy)
	case CapiRke2Deploy:
		return r.applyComponent(
			kanod.Spec.Rke2,
			ctx, env, &kanod, "capi-rke2", resources.Rke2, "", CapiKamajiDeploy)
	case CapiKamajiDeploy:
		return r.applyComponent(
			kanod.Spec.Kamaji,
			ctx, env, &kanod, "capi-kamaji", resources.Kamaji, "", ExternalSecretsDeploy)
	case ExternalSecretsDeploy:
		return r.applyComponent(
			kanod.Spec.ExternalSecrets,
			ctx, env, &kanod, "external-secrets", resources.External, "", BrokerdefDeploy)
	case BrokerdefDeploy:
		if kanod.Spec.BrokerDef != nil {
			env.Setenv("REDFISH_DOMAIN", kanod.Spec.BrokerDef.RedfishDomain)
			if kanod.Spec.BrokerDef.DelegatedAuth {
				env.Setenv("BROKER_MODE", "Auth")
			} else {
				env.Setenv("BROKER_MODE", "Proxy")
			}
			if err := r.configureCredentials(ctx, env, "BDEF", kanod.Namespace, kanod.Spec.BrokerDef.Credentials); err != nil {
				log.Error(err, "Brokerdef credential configuration fails")
				return ctrl.Result{}, err
			}
		}
		return r.applyComponent(
			kanod.Spec.BrokerDef != nil,
			ctx, env, &kanod, "brokerdef", resources.Brokerdef, "", BrokernetDeploy)
	case BrokernetDeploy:
		if kanod.Spec.BrokerNet != nil {
			if err := r.configureCredentials(ctx, env, "BNET", kanod.Namespace, kanod.Spec.BrokerNet.Credentials); err != nil {
				log.Error(err, "Brokernet credential configuration fails")
				return ctrl.Result{}, err
			}
		}
		return r.applyComponent(
			kanod.Spec.BrokerNet != nil,
			ctx, env, &kanod, "brokernet", resources.Brokernet, "", OpenstackDeploy)
	case OpenstackDeploy:
		return r.applyComponent(
			kanod.Spec.CapiHosts && kanod.Spec.CapiHostsOpenstack,
			ctx, env, &kanod, "host-openstack", resources.Openstack, configv1.HostOpenstackReadyCondition, HostClaimDeploy)
	case HostClaimDeploy:
		return r.applyComponent(
			kanod.Spec.CapiHosts,
			ctx, env, &kanod, "HostClaim", resources.HostClaim, configv1.HostClaimReadyCondition, DashboardDeploy)
	case DashboardDeploy:
		return r.applyComponent(
			kanod.Spec.ArgoCD != nil && kanod.Spec.ArgoCD.Dashboard,
			ctx, env, &kanod, "dashboard", resources.Dashboard, "", GitCertsDeploy)
	case GitCertsDeploy:
		if argocd != nil && argocd.GitCerts != nil {
			if err := r.configureGitCerts(ctx, log, kanod.Namespace, argocd.GitCerts); err != nil {
				return ctrl.Result{}, err
			}
		}
		if err := r.setProgression(ctx, &kanod, ConfigureIronicDeployment); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case ConfigureIronicDeployment:
		if broker != nil {
			log.Info("Configure ironic deployment for broker")
			if err := r.ConfigureIronicDeployment(ctx, log); err != nil {
				log.Error(err, "Cannot configure ironic deployment")
				return ctrl.Result{}, err
			}
		}
		if err := r.setProgression(ctx, &kanod, KubevirtDeploy); err != nil {
			log.Error(err, "Cannot set annotation for ironic configuration")
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case KubevirtDeploy:
		return r.applyComponent(
			kanod.Spec.Kubevirt != nil && kanod.Spec.Kubevirt.Enabled,
			ctx, env, &kanod, "Kubevirt", resources.Kubevirt, configv1.KubevirtReadyCondition, NfsProvisionerDeploy)
	case NfsProvisionerDeploy:
		if kanod.Spec.Kubevirt != nil && kanod.Spec.Kubevirt.NfsProvisioner != nil {
			env.Setenv("NFS_SERVER", kanod.Spec.Kubevirt.NfsProvisioner.NfsServer)
			env.Setenv("NFS_PATH", kanod.Spec.Kubevirt.NfsProvisioner.NfsPath)
		}
		return r.applyComponent(
			kanod.Spec.Kubevirt != nil && kanod.Spec.Kubevirt.NfsProvisioner != nil,
			ctx, env, &kanod, "NfsProvisioner", resources.NfsProvisioner, configv1.NfsProvisionerReadyCondition, RookDeploy)
	case RookDeploy:
		return r.applyComponent(
			kanod.Spec.Kubevirt != nil && kanod.Spec.Kubevirt.Rook,
			ctx, env, &kanod, "Rook", resources.Rook, configv1.RookReadyCondition, RookCephDeploy)
	case RookCephDeploy:
		return r.applyComponent(
			kanod.Spec.Kubevirt != nil && kanod.Spec.Kubevirt.Rook,
			ctx, env, &kanod, "RookCeph", resources.RookCeph, "", HostKubevirtDeploy)
	case HostKubevirtDeploy:
		return r.applyComponent(
			kanod.Spec.CapiHosts && kanod.Spec.CapiHostKubevirt,
			ctx, env, &kanod, "host-kubevirt", resources.CapiHostKubevirt, configv1.HostKubevirtReadyCondition, StackWaitStable)
	case StackWaitStable:
		waitFors := []WaitFor{}
		if kanod.Spec.Ironic != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.BaremetalReadyCondition,
				Namespaces:    []string{"baremetal-operator-system"},
			})
			namespaces_bmhdata := []string{
				"bmh-data-system",
			}
			if !kanod.Spec.Capi {
				namespaces_bmhdata = append(namespaces_bmhdata, "capm3-system")
			}
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.BmhDataReadyCondition,
				Namespaces:    namespaces_bmhdata,
			})
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.KeaReadyCondition,
				Namespaces: []string{
					"kanod-kea",
				},
			})
		}
		if kanod.Spec.Capi {
			namespaces := []string{
				"capm3-system",
				"capi-kubeadm-bootstrap-system",
				"capi-kubeadm-control-plane-system",
				"capi-system",
			}
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.ClusterApiReadyCondition,
				Namespaces:    namespaces,
			})
		}
		if kanod.Spec.ArgoCD != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.GitopsReadyCondition,
				Namespaces: []string{
					"argocd",
				},
			})
		}
		if kanod.Spec.ArgoCD != nil && kanod.Spec.ArgoCD.ClusterDef {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.ClusterdefReadyCondition,
				Namespaces: []string{
					"cluster-def-system",
				},
			})
		}
		if kanod.Spec.Ingress != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.IngressReadyCondition,
				Namespaces: []string{
					"ingress-nginx",
					"metallb-system",
				},
			})
		}
		if kanod.Spec.Ironic != nil && kanod.Spec.Ironic.Tpm != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.RegistrarReadyCondition,
				Namespaces: []string{
					"registrar-system",
				},
			})
		}
		if kanod.Spec.CapiHosts {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.HostClaimReadyCondition,
				Namespaces: []string{
					"hostclaim-system",
				},
			})
		}
		if kanod.Spec.CapiHosts && kanod.Spec.Ironic != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.HostBaremetalReadyCondition,
				Namespaces: []string{
					"host-baremetal-system",
				},
			})
		}
		if kanod.Spec.CapiHosts && kanod.Spec.CapiHostsOpenstack {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.OpenstackReadyCondition,
				Namespaces: []string{
					"orc-system",
					"host-openstack-system",
				},
			})
		}
		if kanod.Spec.Broker != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.BaremetalpoolReadyCondition,
				Namespaces: []string{
					"baremetalpool-system",
				},
			})
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.NetworkOperatorReadyCondition,
				Namespaces: []string{
					"network-operator-system",
				},
			})
		}
		if kanod.Spec.Kubevirt != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.KubevirtReadyCondition,
				Namespaces: []string{
					"cdi",
					"kubevirt",
				},
			})
		}

		if kanod.Spec.Rke2 {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.Rke2ReadyCondition,
				Namespaces: []string{
					"rke2-bootstrap-system",
					"rke2-control-plane-system",
				},
			})
		}

		if kanod.Spec.Kubevirt != nil && kanod.Spec.Kubevirt.NfsProvisioner != nil {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.NfsProvisionerReadyCondition,
				Namespaces: []string{
					"nfs-provisioner",
				},
			})
		}

		if kanod.Spec.Kubevirt != nil && kanod.Spec.Kubevirt.Rook {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.RookReadyCondition,
				Namespaces: []string{
					"rook-ceph",
				},
			})
		}

		if kanod.Spec.CapiHosts && kanod.Spec.CapiHostKubevirt {
			waitFors = append(waitFors, WaitFor{
				ConditionType: configv1.HostKubevirtReadyCondition,
				Namespaces: []string{
					"host-kubevirt-system",
				},
			})
		}

		if r.checkDeploymentsAvailable(ctx, &kanod, waitFors) {
			meta.SetStatusCondition(
				&kanod.Status.Conditions,
				metav1.Condition{
					Type:   configv1.KanodStackReadyCondition,
					Status: metav1.ConditionTrue,
					Reason: configv1.ComponentDeployedReason,
				})
			if err := r.updateKanodStatus(&kanod); err != nil {
				return ctrl.Result{}, err
			}
			if err := r.setProgression(ctx, &kanod, ApplicationDeploy); err != nil {
				return ctrl.Result{}, err
			}
		} else {
			if err := r.updateKanodStatus(&kanod); err != nil {
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil

	case ApplicationDeploy:
		if kanod.Spec.ArgoCD != nil {
			argocd := kanod.Spec.ArgoCD
			if err := r.configureApplication(
				ctx, kanod.Namespace, argocd.Baremetal, "baremetals",
				"baremetal-hosts", "BAREMETAL_GIT", resources.AppBaremetals,
			); err != nil {
				return ctrl.Result{}, err
			}

			if err := r.configureApplication(
				ctx, kanod.Namespace, argocd.Projects, "projects",
				"kanod-projects", "PROJECTS_GIT", resources.AppProjects,
			); err != nil {
				return ctrl.Result{}, err
			}

			if err := r.configureApplication(
				ctx, kanod.Namespace, argocd.Network, "network",
				"network-definition", "NETWORK_GIT", resources.AppNetwork,
			); err != nil {
				return ctrl.Result{}, err
			}
		}
		if err := r.setProgression(ctx, &kanod, Final); err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
	case Final:
		log.Info("Reached fixpoint")
		return ctrl.Result{}, nil
	default:
		return ctrl.Result{}, fmt.Errorf("step %d not handled", step)
	}
}

// SetupWithManager sets up the controller with the Manager.
func (r *KanodReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&configv1.Kanod{}).
		Complete(r)
}

func (r *KanodReconciler) applyComponent(
	flag bool,
	ctx context.Context,
	env *Env, kanod *configv1.Kanod,
	name string,
	resource []byte,
	condition string,
	next State,
) (reconcile.Result, error) {
	if flag {
		r.Log.Info("Component deployment.", "component", name)
		if err := r.ApplyData(ctx, env, name, resource); err != nil {
			r.Log.Error(err, "Cannot perform apply for component.", "component", name, "name",
				kanod.Name, "namespace", kanod.Namespace)
			return ctrl.Result{}, err
		}
		if condition != "" {
			meta.SetStatusCondition(
				&kanod.Status.Conditions,
				metav1.Condition{
					Type:   condition,
					Status: metav1.ConditionFalse,
					Reason: configv1.WaitingForComponentReason,
				})
			if err := r.updateKanodStatus(kanod); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		r.Log.Info("Component not deployed.", "component", name)
	}
	if err := r.setProgression(ctx, kanod, next); err != nil {
		r.Log.Error(err, "Cannot set annotation for stack apply", "component", name, "name",
			kanod.Name, "namespace", kanod.Namespace)
		return ctrl.Result{}, err
	}
	return ctrl.Result{RequeueAfter: time.Second * RETRY_DELAY}, nil
}

func (r *KanodReconciler) configureGitCerts(
	ctx context.Context,
	log logr.Logger,
	namespace string,
	gitCerts []configv1.GitCertificates,
) error {
	gitCertsConfigMap := corev1.ConfigMap{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ConfigMap",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "argocd-tls-certs-cm",
			Namespace: "argocd",
		},
	}
	_, err := controllerutil.CreateOrUpdate(
		ctx, r.Client, &gitCertsConfigMap,
		func() error {
			if gitCertsConfigMap.Labels == nil {
				gitCertsConfigMap.Labels = make(map[string]string)
			}
			if gitCertsConfigMap.Data == nil {
				gitCertsConfigMap.Data = make(map[string]string)
			}
			gitCertsConfigMap.Labels["app.kubernetes.io/name"] = "argocd-tls-certs-cm"
			gitCertsConfigMap.Labels["app.kubernetes.io/part-of"] = "argocd"

			for i, gitCert := range gitCerts {
				value, err := r.GetValue(ctx, &gitCert.StringValue, namespace)
				if err != nil {
					log.Error(err, "failed to configure git certificate", "certIndex", i)
					return err
				}
				gitCertsConfigMap.Data[gitCert.Host] = value
			}
			return nil
		})
	return err
}

func (r *KanodReconciler) GetValue(ctx context.Context, sv *configv1.StringValue, namespace string) (string, error) {
	value := sv.Value
	if sv.ValueFrom != nil {
		var err error
		if sv.ValueFrom.ConfigMapName != "" {
			value, err = r.getCMValue(ctx, namespace, sv.ValueFrom)
		} else if sv.ValueFrom.SecretName != "" {
			value, err = r.getSecretValue(ctx, namespace, sv.ValueFrom)
		}
		if err != nil {
			return "", err
		}
	}
	return value, nil
}

func (r *KanodReconciler) getCMValue(ctx context.Context, namespace string, keyRef *configv1.KeyRef) (string, error) {
	var cm corev1.ConfigMap
	objKey := client.ObjectKey{Name: keyRef.ConfigMapName, Namespace: namespace}
	if err := r.Get(ctx, objKey, &cm); err != nil {
		return "", err
	}
	if val, ok := cm.Data[keyRef.Key]; ok {
		return val, nil
	} else {
		return "", fmt.Errorf("getCMValue; no key %s in %s/%s", keyRef.Key, keyRef.ConfigMapName, namespace)
	}
}

func (r *KanodReconciler) getSecretValue(
	ctx context.Context,
	namespace string,
	keyRef *configv1.KeyRef,
) (string, error) {
	var cm corev1.Secret
	objKey := client.ObjectKey{Name: keyRef.SecretName, Namespace: namespace}
	if err := r.Get(ctx, objKey, &cm); err != nil {
		return "", err
	}
	if val, ok := cm.Data[keyRef.Key]; ok {
		return string(val), nil
	} else {
		return "", fmt.Errorf("getSecretValue; no key %s in %s/%s", keyRef.Key, keyRef.SecretName, namespace)
	}
}

func (r *KanodReconciler) configureCredentials(
	ctx context.Context,
	env *Env,
	prefix string,
	namespace string,
	secretName string,
) error {
	var cm corev1.Secret
	objKey := client.ObjectKey{Name: secretName, Namespace: namespace}
	if err := r.Get(ctx, objKey, &cm); err != nil {
		return err
	}
	if val, ok := cm.Data["username"]; ok {
		env.Setenv(fmt.Sprintf("%s_ADMIN_USERNAME_B64", prefix), b64(string(val)))
	} else {
		return fmt.Errorf("cannot find username in secret %s/%s for %s", namespace, secretName, prefix)
	}
	if val, ok := cm.Data["password"]; ok {
		hash, err := bcrypt.GenerateFromPassword(val, bcrypt.DefaultCost)
		if err != nil {
			return err
		}
		env.Setenv(fmt.Sprintf("%s_ADMIN_PASSWORD_B64", prefix), b64(string(hash)))
	} else {
		return fmt.Errorf("cannot find password in secret %s/%s for %s", namespace, secretName, prefix)
	}
	return nil
}

// updateReverseWordsAppStatus updates the Status of a given CR
func (r *KanodReconciler) updateKanodStatus(ka *configv1.Kanod) error {
	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			kanod := &configv1.Kanod{}
			err := r.Get(context.Background(), types.NamespacedName{Name: ka.Name, Namespace: ka.Namespace}, kanod)
			if err != nil {
				return err
			}
			if !reflect.DeepEqual(ka.Status, kanod.Status) {
				r.Log.Info("Updating Status.")
				err = r.Status().Update(context.Background(), ka)
				return err
			}
			return nil
		})
	return err
}

func (r *KanodReconciler) configureCoreDns(ctx context.Context, log logr.Logger, brokerIp string) error {
	//nolint:lll
	cmPatch := `kanod.io.            IN      SOA     sns.dns.icann.org.  noc.dns.icann.org. 2015082541 7200 3600 1209600 3600
kanod.io.            IN      A       %s
*.kanod.io.          IN      A       %s`

	corednsConfigMap := corev1.ConfigMap{
		TypeMeta: metav1.TypeMeta{
			Kind:       "ConfigMap",
			APIVersion: "v1",
		},
		ObjectMeta: metav1.ObjectMeta{
			Name:      "coredns",
			Namespace: "kube-system",
		},
	}
	_, err := controllerutil.CreateOrUpdate(
		ctx, r.Client, &corednsConfigMap,
		func() error {
			if corednsConfigMap.Data == nil {
				corednsConfigMap.Data = make(map[string]string)
			}
			corefileStr := corednsConfigMap.Data["Corefile"]
			corefileStr = strings.ReplaceAll(corefileStr, "loadbalance", "loadbalance\n    file /etc/coredns/kanod.db kanod.io")
			corednsConfigMap.Data["Corefile"] = corefileStr
			corednsConfigMap.Data["kanod.db"] = fmt.Sprintf(cmPatch, brokerIp, brokerIp)

			return nil
		})
	if err != nil {
		log.Error(err, "Cannot patch kanod coredns configmap.")
		return err
	}

	corednsDeployPatch := `{
	"spec": {
		"template": {
			"spec": {
				"volumes": [
					{
					"configMap": {
						"items": [
							{
								"key": "Corefile",
								"path": "Corefile"
							},
							{
								"key": "kanod.db",
								"path": "kanod.db"
							}
						]
					},
					"name": "config-volume"
					}
				]
			}
		}
	}
}
`
	err = r.Client.Patch(
		ctx,
		&appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "coredns",
				Namespace: "kube-system",
			},
		},
		client.RawPatch(types.StrategicMergePatchType, []byte(corednsDeployPatch)),
	)
	if err != nil {
		log.Error(err, "Cannot patch coredns deployment.")
	}

	return err
}

func (r *KanodReconciler) ConfigureIronicDeployment(ctx context.Context, log logr.Logger) error {
	ironicDeployPatch := `{"spec":{"template":{"spec":{"dnsPolicy": "ClusterFirstWithHostNet"}}}}`
	err := r.Client.Patch(
		ctx,
		&appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      "ironic",
				Namespace: "baremetal-operator-system",
			},
		},
		client.RawPatch(types.StrategicMergePatchType, []byte(ironicDeployPatch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot patch ironic deployment.")
	}

	return err
}

func (r *KanodReconciler) configureApplication(
	ctx context.Context,
	namespace string,
	project *configv1.ArgoProject,
	kind string, appName string,
	variable string,
	resource []byte,
) error {
	if project == nil {
		r.Log.Info("No application for this kind", "kind", kind)
		return nil
	}
	var cred corev1.Secret
	objKey := client.ObjectKey{Name: project.Credentials, Namespace: namespace}
	if err := r.Get(ctx, objKey, &cred); err != nil {
		return err
	}
	data := cred.DeepCopy().Data
	data["url"] = []byte(project.Url)
	secret := corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("git-%s-credentials", kind),
			Namespace: "argocd",
			Labels:    map[string]string{"argocd.argoproj.io/secret-type": "repository"},
		},
		Data: data,
	}

	_, err := controllerutil.CreateOrUpdate(
		ctx, r.Client, &secret,
		func() error {
			if secret.ObjectMeta.Labels == nil {
				secret.ObjectMeta.Labels = map[string]string{}
			}
			secret.ObjectMeta.Labels["argocd.argoproj.io/secret-type"] = "repository"
			secret.Data = data
			if secret.Data == nil {
				secret.Data = data
			}
			return nil
		})
	if err != nil {
		r.Log.Error(err, "Cannot create git secret", "kind", kind, "name", appName)
		return err
	}
	env := Env{Env: make(map[string]string)}
	env.Setenv(variable, project.Url)
	if err := r.ApplyData(ctx, &env, kind, resource); err != nil {
		r.Log.Error(err, "Cannot create application", "kind", kind, "name", appName)
		return err
	}
	if r.Config.VaultUrl != "" {
		if err := r.AddApplicationPlugin(ctx, appName); err != nil {
			r.Log.Error(err, "Cannot specialize app for Vault.", "kind", kind)
			return err
		}
	}
	return nil
}
