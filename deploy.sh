export NEXUS_KANOD_USER=admin
export NEXUS_KANOD_PASSWORD=secret
export NEXUS_REGISTRY=registry.service.kanod.home.arpa
export REPO_URL=http://maven.service.kanod.home.arpa/repository/kanod
export VERSION=ext
export MAVEN_OPTS=''
export MAVEN_CLI_OPTS=''
export NO_IMAGE_SYNC=1
./build.sh
