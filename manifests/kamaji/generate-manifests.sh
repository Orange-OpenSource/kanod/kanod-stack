#!/bin/bash

set -eu
set -o pipefail

export CHART_RELEASE=${1:-0.12.3}
export PROVIDER_RELEASE=${2:-v0.3.0}
export HELM_RELEASE=v3.12.3
export KUSTOMIZE_RELEASE=v5.1.1

BASE_DIR=$(readlink -f "$(dirname "${BASH_SOURCE[0]}")")

temp=$(mktemp -d)

echo "fetching helm version ${HELM_RELEASE}"
curl --fail -L  "https://get.helm.sh/helm-${HELM_RELEASE}-linux-amd64.tar.gz" | tar xvfz - -C "${temp}" linux-amd64/helm

echo "fetching kamaji with helm chart version ${CHART_RELEASE}"
"${temp}"/linux-amd64/helm repo add clastix https://clastix.github.io/charts
"${temp}"/linux-amd64/helm repo update
"${temp}"/linux-amd64/helm template kamaji clastix/kamaji --include-crds --version "${CHART_RELEASE}" -n kamaji-system > "${BASE_DIR}"/kamaji.yaml

echo "fetching kamaji capi provider version ${PROVIDER_RELEASE}"
curl --fail -L -o "${BASE_DIR}/kamaji-control-plane-components.yaml" \
    "https://github.com/clastix/cluster-api-control-plane-provider-kamaji/releases/download/${PROVIDER_RELEASE}/control-plane-components.yaml"

rm -fr "${temp}"
