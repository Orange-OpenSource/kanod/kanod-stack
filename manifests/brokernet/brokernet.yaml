apiVersion: v1
kind: Namespace
metadata:
  labels:
    control-plane: controller-manager
  name: brokernet-system
---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.4.1
  creationTimestamp: null
  name: networkattachments.brokernet.kanod.io
spec:
  group: brokernet.kanod.io
  names:
    kind: NetworkAttachment
    listKind: NetworkAttachmentList
    plural: networkattachments
    singular: networkattachment
  scope: Namespaced
  versions:
  - name: v1
    schema:
      openAPIV3Schema:
        description: NetworkAttachment is the Schema for the networkattachments API
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: NetworkAttachmentSpec defines the desired state of Networkattachment
            properties:
              hostInterface:
                description: Physical network interface on the host
                type: string
              hostName:
                description: Name of the host
                type: string
              networkDefinitions:
                additionalProperties:
                  type: string
                description: List of NetworkDefinitions
                type: object
              switchName:
                description: Switch name linked to the host
                type: string
              switchPort:
                description: Switch port connected to the host
                type: string
            required:
            - hostInterface
            - hostName
            - switchName
            - switchPort
            type: object
          status:
            description: NetworkAttachmentStatus defines the observed state of Networkattachment
            properties:
              state:
                type: string
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.4.1
  creationTimestamp: null
  name: networkdefinitions.brokernet.kanod.io
spec:
  group: brokernet.kanod.io
  names:
    kind: NetworkDefinition
    listKind: NetworkDefinitionList
    plural: networkdefinitions
    singular: networkdefinition
  scope: Namespaced
  versions:
  - name: v1
    schema:
      openAPIV3Schema:
        description: NetworkDefinition is the Schema for the NetworkDefinitions API
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: NetworkDefinitionSpec defines the desired state of NetworkDefinition
            properties:
              credentialName:
                description: Name of the secret associated to this network definition
                type: string
              dhcpMode:
                description: DhcpMode
                nullable: true
                type: string
              dhcpServerIp:
                description: Subnet DHCP server address
                type: string
              domainNameServers:
                description: DomainNameServers
                items:
                  type: string
                type: array
              gatewayIp:
                description: Gateway IP/mask address
                type: string
              subnetEnd:
                description: Subnet range end
                type: string
              subnetPrefix:
                description: Subnet IP/mask address
                type: string
              subnetStart:
                description: Subnet range start
                type: string
              type:
                description: Virtual network type
                type: string
              vnid:
                description: Virtual network identifier
                type: string
            required:
            - credentialName
            - dhcpMode
            - dhcpServerIp
            - subnetEnd
            - subnetPrefix
            - subnetStart
            - type
            - vnid
            type: object
          status:
            description: NetworkDefinitionStatus defines the observed state of NetworkDefinition
            properties:
              state:
                description: Status
                type: string
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.4.1
  creationTimestamp: null
  name: switches.brokernet.kanod.io
spec:
  group: brokernet.kanod.io
  names:
    kind: Switch
    listKind: SwitchList
    plural: switches
    singular: switch
  scope: Namespaced
  versions:
  - name: v1
    schema:
      openAPIV3Schema:
        description: Switch is the Schema for the switches API
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: SwitchSpec defines the desired state of Switch
            properties:
              credentials:
                description: Name of the secret associated to this switch
                type: string
              ipAddress:
                description: IP address of the management interface
                type: string
              links:
                description: Links
                properties:
                  switches:
                    description: Switches
                    items:
                      properties:
                        name:
                          description: Name
                          type: string
                        port:
                          description: Port
                          type: string
                      type: object
                    type: array
                required:
                - switches
                type: object
              name:
                description: Switch name
                type: string
              vendor:
                description: Switch vendor information
                type: string
            required:
            - credentials
            - ipAddress
            - name
            type: object
          status:
            description: SwitchStatus defines the observed state of Switch
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: brokernet-controller-manager
  namespace: brokernet-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: brokernet-leader-election-role
  namespace: brokernet-system
rules:
- apiGroups:
  - ""
  resources:
  - configmaps
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch
  - delete
- apiGroups:
  - coordination.k8s.io
  resources:
  - leases
  verbs:
  - get
  - list
  - watch
  - create
  - update
  - patch
  - delete
- apiGroups:
  - ""
  resources:
  - events
  verbs:
  - create
  - patch
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  creationTimestamp: null
  name: brokernet-manager-role
rules:
- apiGroups:
  - ""
  resources:
  - secrets
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - broker.kanod.io
  resources:
  - baremetaldefs
  verbs:
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - broker.kanod.io
  resources:
  - pooldefs
  verbs:
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - brokernet.kanod.io
  resources:
  - networkattachments
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - brokernet.kanod.io
  resources:
  - networkattachments/finalizers
  verbs:
  - update
- apiGroups:
  - brokernet.kanod.io
  resources:
  - networkattachments/status
  verbs:
  - get
  - patch
  - update
- apiGroups:
  - brokernet.kanod.io
  resources:
  - networkdefinitions
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - brokernet.kanod.io
  resources:
  - networkdefinitions/finalizers
  verbs:
  - update
- apiGroups:
  - brokernet.kanod.io
  resources:
  - networkdefinitions/status
  verbs:
  - get
  - patch
  - update
- apiGroups:
  - brokernet.kanod.io
  resources:
  - switches
  verbs:
  - create
  - delete
  - get
  - list
  - patch
  - update
  - watch
- apiGroups:
  - brokernet.kanod.io
  resources:
  - switches/finalizers
  verbs:
  - update
- apiGroups:
  - brokernet.kanod.io
  resources:
  - switches/status
  verbs:
  - get
  - patch
  - update
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: brokernet-metrics-reader
rules:
- nonResourceURLs:
  - /metrics
  verbs:
  - get
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: brokernet-proxy-role
rules:
- apiGroups:
  - authentication.k8s.io
  resources:
  - tokenreviews
  verbs:
  - create
- apiGroups:
  - authorization.k8s.io
  resources:
  - subjectaccessreviews
  verbs:
  - create
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: brokernet-leader-election-rolebinding
  namespace: brokernet-system
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: brokernet-leader-election-role
subjects:
- kind: ServiceAccount
  name: brokernet-controller-manager
  namespace: brokernet-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: brokernet-manager-rolebinding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: brokernet-manager-role
subjects:
- kind: ServiceAccount
  name: brokernet-controller-manager
  namespace: brokernet-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: brokernet-proxy-rolebinding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: brokernet-proxy-role
subjects:
- kind: ServiceAccount
  name: brokernet-controller-manager
  namespace: brokernet-system
---
apiVersion: v1
data:
  controller_manager_config.yaml: |
    apiVersion: controller-runtime.sigs.k8s.io/v1alpha1
    kind: ControllerManagerConfig
    health:
      healthProbeBindAddress: :8081
    metrics:
      bindAddress: 127.0.0.1:8080
    webhook:
      port: 9443
    leaderElection:
      leaderElect: true
      resourceName: 477a38d7.kanod.io
kind: ConfigMap
metadata:
  name: brokernet-manager-config
  namespace: brokernet-system
---
apiVersion: v1
data:
  password: ${BNET_ADMIN_PASSWORD_B64}
  username: ${BNET_ADMIN_USERNAME_B64}
kind: Secret
metadata:
  name: brokernet-netdef-admin
  namespace: brokernet-system
type: Opaque
---
apiVersion: v1
kind: Service
metadata:
  name: brokernet-brokernet-api
  namespace: brokernet-system
spec:
  ports:
  - nodePort: 30021
    port: 9001
    protocol: TCP
    targetPort: 9001
  selector:
    control-plane: controller-manager
  type: NodePort
---
apiVersion: v1
kind: Service
metadata:
  labels:
    control-plane: controller-manager
  name: brokernet-controller-manager-metrics-service
  namespace: brokernet-system
spec:
  ports:
  - name: https
    port: 8443
    targetPort: https
  selector:
    control-plane: controller-manager
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    control-plane: controller-manager
  name: brokernet-controller-manager
  namespace: brokernet-system
spec:
  replicas: 1
  selector:
    matchLabels:
      control-plane: controller-manager
  template:
    metadata:
      labels:
        control-plane: controller-manager
    spec:
      containers:
      - args:
        - --secure-listen-address=0.0.0.0:8443
        - --upstream=http://127.0.0.1:8080/
        - --logtostderr=true
        - --v=10
        image: gcr.io/kubebuilder/kube-rbac-proxy:v0.8.0
        name: kube-rbac-proxy
        ports:
        - containerPort: 8443
          name: https
      - args:
        - --health-probe-bind-address=:8081
        - --metrics-bind-address=127.0.0.1:8080
        - --leader-elect
        command:
        - /manager
        env:
        - name: ADMIN_USERNAME
          valueFrom:
            secretKeyRef:
              key: username
              name: brokernet-netdef-admin
        - name: ADMIN_PASSWORD
          valueFrom:
            secretKeyRef:
              key: password
              name: brokernet-netdef-admin
        image: registry.gitlab.com/orange-opensource/kanod/brokernet:v0.1.0
        livenessProbe:
          httpGet:
            path: /healthz
            port: 8081
          initialDelaySeconds: 15
          periodSeconds: 20
        name: manager
        ports:
        - containerPort: 9001
        readinessProbe:
          httpGet:
            path: /readyz
            port: 8081
          initialDelaySeconds: 5
          periodSeconds: 10
        resources:
          limits:
            cpu: 100m
            memory: 30Mi
          requests:
            cpu: 100m
            memory: 20Mi
        securityContext:
          allowPrivilegeEscalation: false
      securityContext:
        runAsNonRoot: true
      serviceAccountName: brokernet-controller-manager
      terminationGracePeriodSeconds: 10
