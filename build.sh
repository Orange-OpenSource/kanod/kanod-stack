#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -eu

for var in NEXUS_KANOD_USER NEXUS_REGISTRY REPO_URL VERSION
do
    if ! [[ -v "${var}" ]]; then
        echo "${var} must be defined"
        exit
    fi
done


export STACK_VERSION=${VERSION}

bash ci-build.sh

# shellcheck disable=SC1090,SC1091
if [[ -v KANOD_VENV ]]; then
    source "$KANOD_VENV/bin/activate"
fi

export TAG="${STACK_VERSION%.*([0-9])}"

if [ "${NO_IMAGE_SYNC:-0}" == 0 ]; then
  bash ./ci-sync.sh
fi

bash ./ci-upload.sh
./ironic-image-container.sh
./kanod-operator-container.sh

if [ "${KANOD_PRUNE_IMAGES:-0}" == "1" ]; then
    docker image prune -a --force --filter 'label=project=kanod-operator'
fi
