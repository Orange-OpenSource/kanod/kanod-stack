#! /bin/sh

#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
set -eu

IRONIC_RELEASE=v27.0.0
mirror=''
if [ -n "${REGISTRY_MIRROR:-}" ]; then
    mirror="--skip-tls-verify-registry ${REGISTRY_MIRROR} --registry-mirror ${REGISTRY_MIRROR}"
fi

mkdir -p /kaniko/.docker
echo "{\"auths\":{\"${REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${REGISTRY_USER}" "${REGISTRY_PASSWORD}" | base64 | tr -d "\n")\"}}}" > /kaniko/.docker/config.json 

# shellcheck disable=SC2086
/kaniko/executor --insecure ${mirror} \
    --build-arg http_proxy --build-arg https_proxy --build-arg no_proxy --build-arg "IRONIC_VERSION=${IRONIC_RELEASE}"\
    --context dir:///app/build/ironic-image\
    --destination "${REGISTRY}/orange-opensource/kanod/kanod-stack/ironic-image:${STACK_VERSION}"
