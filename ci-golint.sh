#!/bin/bash

cd kanod-operator || exit

grep "go:embed" resources/resources.go | cut -d " " -f 2 | while IFS= read -r yaml_file
do
  touch "resources/$yaml_file"
done

make lint build
